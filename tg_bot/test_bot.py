import pytest
from aiogram.filters.callback_data import CallbackData
import pandas as pd
from main import request_ticker, fin_service
from main import set_ticker
from main import show_history
from main import sendgraph
from main import predict
from unittest.mock import AsyncMock
from aiogram.fsm.context import FSMContext
from aiogram_tests import MockedBot
from aiogram_tests.handler import MessageHandler
from aiogram_tests.types.dataset import MESSAGE
from main import StocksState
from main import FinService
from main import FinanceModel
from aiogram.fsm.state import State
from aiogram.fsm.state import StatesGroup
from aiogram_tests.handler import CallbackQueryHandler
from aiogram_tests.types.dataset import CALLBACK_QUERY
import yfinance as yf
class FinServiceTest:

    def get_history(self, ticker_name, period):
        if period == '90d':
            data_history = yf.download(ticker_name, period='90d')
        elif period=='max':
            data_history = yf.download(ticker_name, period='max')
        elif period=='yesterday':
            data_history = yf.download(ticker_name, period='yesterday')
# file peredat'
        return data_history

fin_service_test = FinServiceTest()

@pytest.mark.asyncio
async def test_request_ticker():
    requester = MockedBot(MessageHandler(request_ticker))
    calls = await requester.query(MESSAGE.as_object(text="Please write ticker"))
    answer_message = calls.send_message.fetchone().text
    assert answer_message == "Please write ticker"


class TestCallbackData(CallbackData, prefix="test_callback_data"):
    ticker: str


@pytest.mark.asyncio
async def test_set_ticker():
    requester = MockedBot(MessageHandler(set_ticker))
    calls = await requester.query(MESSAGE.as_object(text="AAPL"))
    answer_message = calls.send_message.fetchone().text
    assert answer_message == "You chose ticker AAPL"

@pytest.mark.asyncio
async def test_show_history():
    requester = MockedBot(
        MessageHandler(
            show_history, state=StocksState.stock_name, state_data={'stock_name': 'AAPL'}, finance_service=fin_service_test
            )
        )
    calls = await requester.query(MESSAGE.as_object(text="AAPL"))
    answer_message = calls.send_message.fetchone().text
    df = fin_service_test.get_history('AAPL','yesterday')
    assert answer_message == (f"Historical data for AAPL yesterday:\n"
                              f"Close\n"
                              f"{round(df['Close'].values[0], 2)}\n")


@pytest.mark.asyncio
async def test_graph():
    requester = MockedBot(
        MessageHandler(
            sendgraph, state=StocksState.stock_name, state_data={'stock_name': 'AAPL'}, finance_service=fin_service_test
        )
    )
    calls = await requester.query(MESSAGE.as_object(text='/graph'))
    answer_message = calls.send_photo.fetchone()
    with open('graph1.png', 'rb') as f:
        expected_bytes = f.read()
    assert answer_message.photo.data == expected_bytes


@pytest.mark.asyncio
async def test_predict():
    requester = MockedBot(
        MessageHandler(
            predict, state=StocksState.stock_name, state_data={'stock_name': 'AAPL'}, finance_service=fin_service_test
        )
    )
    calls = await requester.query(MESSAGE.as_object(text='/predict'))
    answer_message = calls.send_message.fetchone().text
    df = fin_service_test.get_history('AAPL', '90d')
    fin_model = FinanceModel()
    fin_model.import_model('model_1.pkl')
    X_df = fin_model.create_featured_df(df)
    future_price = fin_model.predict(X_df)
    assert answer_message == f'tomorrow price is {round(future_price[0], 2)}'

