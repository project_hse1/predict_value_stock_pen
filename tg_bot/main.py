import asyncio
import base64
import gzip
import logging
import os
import pickle

from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.types import ReplyKeyboardMarkup, BufferedInputFile
from aiogram import Bot, Dispatcher, types, F, Router
from aiogram.filters.command import Command, CommandStart
from datetime import datetime, timedelta
from aiogram.methods.send_photo import SendPhoto
import io
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import yfinance as yf
from pickle import load
import joblib
import requests

router = Router()


class FinanceModel:

    def __init__(self):
        self.model = None
        self.standart = None

    def import_model(self, file_path):
        with open(file_path, 'rb') as f:
            self.model = joblib.load(f)


    def prepare_df(self, df):
        _df = df
        _df['future_price'] = _df['Close'].shift(-1) / _df['Close']

        return _df

    def create_features(self, _df):
        # создание новых признаков
        # среднее занчения за 3 5 7 14 30 90 дней
        _df['rolling_3D_Open_mean'] = _df['Open'].rolling(window=3).mean()
        _df['rolling_5D_Open_mean'] = _df['Open'].rolling(window=5).mean()
        _df['rolling_week_Open_mean'] = _df['Open'].rolling(window=7).mean()
        _df['rolling_2weeks_Open_mean'] = _df['Open'].rolling(window=14).mean()
        _df['rolling_mounth_Open_mean'] = _df['Open'].rolling(window=30).mean()
        _df['rolling_quartal_Open_mean'] = _df['Open'].rolling(window=90).mean()

        _df['rolling_3D_Close_mean'] = _df['Close'].rolling(window=3).mean()
        _df['rolling_5D_Close_mean'] = _df['Close'].rolling(window=5).mean()
        _df['rolling_week_Close_mean'] = _df['Close'].rolling(window=7).mean()
        _df['rolling_2weeks_Close_mean'] = _df['Close'].rolling(window=14).mean()
        _df['rolling_mounth_Close_mean'] = _df['Close'].rolling(window=30).mean()
        _df['rolling_quartal_Close_mean'] = _df['Close'].rolling(window=90).mean()

        _df['rolling_3D_High_mean'] = _df['High'].rolling(window=3).mean()
        _df['rolling_5D_High_mean'] = _df['High'].rolling(window=5).mean()
        _df['rolling_week_High_mean'] = _df['High'].rolling(window=7).mean()
        _df['rolling_2weeks_High_mean'] = _df['High'].rolling(window=14).mean()
        _df['rolling_mounth_High_mean'] = _df['High'].rolling(window=30).mean()
        _df['rolling_quartal_High_mean'] = _df['High'].rolling(window=90).mean()

        _df['rolling_3D_Low_mean'] = _df['Low'].rolling(window=3).mean()
        _df['rolling_5D_Low_mean'] = _df['Low'].rolling(window=5).mean()
        _df['rolling_week_Low_mean'] = _df['Low'].rolling(window=7).mean()
        _df['rolling_2weeks_Low_mean'] = _df['Low'].rolling(window=14).mean()
        _df['rolling_mounth_Low_mean'] = _df['Low'].rolling(window=30).mean()
        _df['rolling_quartal_Low_mean'] = _df['Low'].rolling(window=90).mean()
        #     дисперсия по окнам за 3 5 7 14 30 90 дней
        _df['disp_3D_Open'] = _df['Open'].rolling(window=3).var()
        _df['disp_5D_Open'] = _df['Open'].rolling(window=5).var()
        _df['disp_week_Open'] = _df['Open'].rolling(window=7).var()
        _df['disp_2weeks_Open'] = _df['Open'].rolling(window=14).var()
        _df['disp_mounth_Open'] = _df['Open'].rolling(window=30).var()
        _df['disp_quartal_Open'] = _df['Open'].rolling(window=90).var()

        _df['disp_3D_Close'] = _df['Close'].rolling(window=3).var()
        _df['disp_5D_Close'] = _df['Close'].rolling(window=5).var()
        _df['disp_week_Close'] = _df['Close'].rolling(window=7).var()
        _df['disp_2weeks_Close'] = _df['Close'].rolling(window=14).var()
        _df['disp_mounth_Close'] = _df['Close'].rolling(window=30).var()
        _df['disp_quartal_Close'] = _df['Close'].rolling(window=90).var()

        _df['disp_3D_High'] = _df['High'].rolling(window=3).var()
        _df['disp_5D_High'] = _df['High'].rolling(window=5).var()
        _df['disp_week_High'] = _df['High'].rolling(window=7).var()
        _df['disp_2weeks_High'] = _df['High'].rolling(window=14).var()
        _df['disp_mounth_High'] = _df['High'].rolling(window=30).var()
        _df['disp_quartal_High'] = _df['High'].rolling(window=90).var()

        _df['disp_3D_Low'] = _df['Low'].rolling(window=3).var()
        _df['disp_5D_Low'] = _df['Low'].rolling(window=5).var()
        _df['disp_week_Low'] = _df['Low'].rolling(window=7).var()
        _df['disp_2weeks_Low'] = _df['Low'].rolling(window=14).var()
        _df['disp_mounth_Low'] = _df['Low'].rolling(window=30).var()
        _df['disp_quartal_Low'] = _df['Low'].rolling(window=90).var()
        # относительное изменение за день
        _df['relative_change_Open'] = _df['Open'] / _df['Open'].shift(1)
        _df['relative_change_Close'] = _df['Close'] / _df['Close'].shift(1)
        _df['relative_change_High'] = _df['High'] / _df['High'].shift(1)
        _df['relative_change_Low'] = _df['Low'] / _df['Low'].shift(1)
        #   вычисление логарифмической доходности
        _df['Log_Returns'] = np.log(_df['Close'] / _df['Open'])
        #   Стандартное отклонение
        _df['Volatility_3D'] = _df['Log_Returns'].rolling(window=3).std()
        _df['Volatility_5D'] = _df['Log_Returns'].rolling(window=5).std()
        _df['Volatility_week'] = _df['Log_Returns'].rolling(window=7).std()
        _df['Volatility_2week'] = _df['Log_Returns'].rolling(window=14).std()
        _df['Volatility_mounth'] = _df['Log_Returns'].rolling(window=30).std()
        _df['Volatility_quartal'] = _df['Log_Returns'].rolling(window=90).std()
        #   изменение цены акции
        _df['Price Change'] = _df['Close'].diff()
        # период RSI
        n = 14
        # положительное и отрицательное изменение цены
        _df['Gain'] = _df['Price Change'].apply(lambda x: x if x > 0 else 0)
        _df['Loss'] = _df['Price Change'].apply(lambda x: -x if x < 0 else 0)
        # среднее значение изменения цены для заданного периода
        _df['Avg Gain'] = _df['Gain'].rolling(window=n).mean()
        _df['Avg Loss'] = _df['Loss'].rolling(window=n).mean()
        # отношение среднего прироста к среднему падению
        relative_strength = _df['Avg Gain'] / _df['Avg Loss']
        # RSI
        _df['RSI'] = 100 - (100 / (1 + relative_strength))
        #    ежедневное изменение цены в %
        _df['% daily price change'] = _df['Close'].pct_change() * 100

        return _df

    def create_featured_df(self, df):
        _df = self.prepare_df(df)
        X_f = self.create_features(_df)
        X_f = X_f.loc[:, X_f.columns != 'future_price']
        X_f = X_f.dropna()
        return X_f

    def predict(self, X_df):
        predicted_percent = self.model.predict(X_df)
        future_price = predicted_percent[0] * X_df['Close']
        return future_price


class FinService:

    def get_history(self, ticker_name, period='max'):
        data_history = yf.download(ticker_name, period=period)

        return data_history


class StocksState(StatesGroup):
    stock_name = State()

fin_service = FinService()


class FastApiService:
    def __init__(self):
        self.host = os.getenv('API_HOST')
    def get_predict(self, ticker):
        response = requests.get(f'{self.host}/predict/{ticker}')
        return response.json()
    def get_history(self, ticker):
        response = requests.get(f'{self.host}/history/{ticker}')
        return response.json()

    def get_graph(self, ticker):
        response = requests.get(f'{self.host}/graph/{ticker}')
        return response.content

fastapi_fin_service = FastApiService()

@router.message(Command('set_ticker'))
async def request_ticker(message: types.Message, state: FSMContext):
    await message.answer('Please write ticker')
    await state.set_state(StocksState.stock_name)


@router.message(StocksState.stock_name)
async def set_ticker(message: types.Message, state: FSMContext):
    await state.update_data(stock_name=message.text)
    state_data = await state.get_data()
    ticker = state_data['stock_name']
    await message.answer(f'You chose ticker {ticker}')
    await state.set_state(None)


@router.message(Command('history'))
async def show_history(message: types.Message, state: FSMContext):
    state_data = await state.get_data()
    ticker = state_data['stock_name']
    finance_service = FastApiService()
    history_data = finance_service.get_history(ticker)

    message_text = f"Historical data for {ticker}:\n"
    message_text += (f"Close price: {history_data['price']}")

    await message.answer(message_text)


@router.message(Command('graph'))
async def sendgraph(message: types.Message, state: FSMContext, finance_service=fastapi_fin_service):
    state_data = await state.get_data()
    ticker = state_data['stock_name']
    data = finance_service.get_graph(ticker)


    await message.answer_photo(BufferedInputFile(data, "graph1.png"))


@router.message(Command('predict'))
async def predict(message: types.Message, state: FSMContext):
    state_data = await state.get_data()
    ticker = state_data['stock_name']
    finance_service = FastApiService()
    predict_data = finance_service.get_predict(ticker)

    await message.answer(f'tomorrow price is {predict_data["future_price"]}')





async def main():
    logging.basicConfig(level=logging.INFO)
    bot = Bot(token=os.getenv('TG_TOKEN'))
    dp = Dispatcher()
    dp.include_router(router)
    await dp.start_polling(bot, skip_updates=True)


if __name__ == "__main__":
    asyncio.run(main())
