FROM python:3.11-slim
COPY ./requirements.txt /service/requirements.txt
RUN pip install -r /service/requirements.txt
COPY . /service
WORKDIR /service
CMD ["python", "main.py"]
# CMD ["uvicorn", "app.main:app"]
